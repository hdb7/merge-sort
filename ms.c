#include<stdio.h>

void merge(int *a, int l, int m, int h)
{
    int i,j,k,f_half,s_half;
    f_half=m-l+1;
    s_half=h-m;
    int L[f_half],R[s_half];
    for(i=0;i<f_half;i++)
        L[i]=a[l+i];
    for(j=0;j<s_half;j++)
        R[j]=a[m+1+j]; 
    i=j=0;
    k=l;
    while(i<f_half && j<s_half){
        if(L[i]<R[j]){
            a[k]=L[i];
            i++;
        }else{
            a[k]=R[j];
            j++;
        }
        k++;
    }
    while(i<f_half){
        a[k]=L[i];
        i++;k++;
    }
    while(j<s_half){
        a[k]=R[j];
        j++;k++;
    }
}
void m_sort(int *A, int low, int high)
{
    if(low<high){
        int mid;
        mid=low+(high-low)/2;
        m_sort(A,low,mid);
        m_sort(A,mid+1,high);
        merge(A,low,mid,high);
    }
}
int main()
{
    int i,arr_len;
    int arr[]={12, 11, 13, 5, 6, 7};
    arr_len=sizeof(arr)/sizeof(arr[0]);
    m_sort(arr,0,arr_len-1);
    for(i=0;i<arr_len;i++)
        printf("%d ",arr[i]);
    printf("\n");
    return 0;
}